#!/usr/bin/python

import subprocess
import os
import shutil
import sys

cfg = {}
cfg['svnuser'] = 'CiaranG'
cfg['svnrepo'] = 'https://plugins.svn.wordpress.org/wp-minibar/'
# Plugin files - first one should be main plugin php file
cfg['files'] = ['wp-minibar.php', 'readme.txt', 'minibar.js']


if os.path.exists('svn'):
    shutil.rmtree('svn')
if subprocess.call(['svn', 'co', '--username', cfg['svnuser'], 
        cfg['svnrepo'], 'svn']) != 0:
    print "Checkout failed"
    sys.exit(1)

version_php = None
with open(cfg['files'][0]) as f:
    for line in f:
        if line.startswith('Version: '):
            version_php = line[9:].rstrip()
            break
if not version_php:
    print "Couldn't find version in main php file"
    sys.exit(1)

version_readme = None
with open('readme.txt') as f:
    for line in f:
        if line.startswith('Stable tag: '):
            version_readme = line[12:].rstrip()
            break
if not version_readme:
    print "Couldn't find version in readme file"
    sys.exit(1)

if version_php != version_readme:
    print "Version in main php and readme file should match"
    sys.exit(1)

# Check this is a new version
tagdir = os.path.join('svn', 'tags', version_php)
if os.path.exists(tagdir):
    print "Version " + version_php + " is already tagged!"
    sys.exit(1)

print "Deploying version '" + version_php + "'"

# Update trunk
print "...updating trunk"
trunkdir = os.path.join('svn', 'trunk')
#TODO: Need to remove files that no longer exist, without clobbering .svn
for fn in cfg['files']:
    shutil.copyfile(fn, os.path.join(trunkdir, fn))

# Commit trunk changes
print "...commiting to trunk"
if subprocess.call('svn add * --force', shell=True,
        cwd='svn') != 0:
    print "Failed to svn add"
    sys.exit(1)
if subprocess.call(['svn', 'commit', '--username', cfg['svnuser'],
        '-m', "Changes for version " + version_php, '.'],
        cwd=trunkdir) != 0:
    print "Failed to commit"
    sys.exit(1)

# Update tag
if subprocess.call(['svn', 'copy', 'trunk', os.path.join('tags', version_php)],
        cwd='svn') != 0:
    print "Failed to create tag"
    sys.exit(1)

# Commit new tag
print "...creating new tag"
if subprocess.call(['svn', 'commit', '--username', cfg['svnuser'],
        '-m', "Tagging version " + version_php],
        cwd='svn') != 0:
    print "Failed to commit"
    sys.exit(1)

shutil.rmtree('svn')


